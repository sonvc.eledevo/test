import React, { Component } from "react";
import { View } from 'react-native'
import { connect } from "react-redux";
import OtpComponent from "../components/OtpComponent";

class OtpContainer extends Component {
    render() {
        return (
            <View>
                <OtpComponent {...this.props} />
            </View>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        Login: (data) => {
            dispatch(actions.registerRequest(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(OtpContainer)