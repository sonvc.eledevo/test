import React, { Component } from "react";
import { View, Text } from 'react-native'
import SalaryComponent from "../components/SalaryComponent";
export default class SalaryContainer extends Component {
    render() {
        return (
            <View>
                <SalaryComponent {...this.props} />
            </View>
        )
    }
}

