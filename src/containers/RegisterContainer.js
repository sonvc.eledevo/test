import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import * as actions from "../actions/loginAction";
import RegisterComponent from "../components/RegisterComponent";

class RegisterContainer extends Component {
  render() {
    return (
      <View>
        <RegisterComponent {...this.props} />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Login: (data) => {
      dispatch(actions.registerRequest(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(RegisterContainer);
