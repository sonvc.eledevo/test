import React, { Component } from "react";
import { View } from 'react-native'
import { connect } from "react-redux";
import NewPasswordComponent from "../components/NewPasswordComponent";
import * as actions from '../actions/newPassword'

class NewPasswordContainer extends Component {
    render() {
        return (
            <View>
                <NewPasswordComponent {...this.props} />
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        NewPassword: (data) => {
            dispatch(actions.newPassRequest(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(NewPasswordContainer)