import React, { Component } from "react";
import { View } from 'react-native'
import { connect } from "react-redux";
import ForgotPasswordComponent from '../components/ForgotPasswordComponent'
import * as actions from '../actions/forgotPassword'

class ForgotPasswordContainer extends Component {
    render() {
        return (
            <View>
                <ForgotPasswordComponent {...this.props} />
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        ForgotPass: (data) => {
            dispatch(actions.registerRequest(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(ForgotPasswordContainer)