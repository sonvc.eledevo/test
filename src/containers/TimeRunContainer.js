import React, { Component } from "react";
import { View } from 'react-native'
import TimeRunComponent from '../components/TimeRunComponent'


export default class TimeRunContainer extends Component {
    render() {
        return (
            <View>
                <TimeRunComponent {...this.props} />
            </View>
        )
    }
}
