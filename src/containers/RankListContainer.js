import React, { Component } from "react";
import { View, Text } from 'react-native'
import RankListComponent from "../components/RankListComponent";

export default class RankListContainer extends Component {
    render() {
        return (
            <View>
                <RankListComponent {...this.props} />
            </View>
        )
    }
}

