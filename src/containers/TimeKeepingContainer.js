import React, { Component } from "react";
import { View, Text } from "react-native";
import TimeKeepingComponent from "../components/TimeKeepingComponent";

export default class TimeKeepingContainer extends Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          padding: 5,
          borderRadius:10,
          // height:60,
          // borderBottomColor: "gray",
          // borderBottomWidth: 0.5,
          shadowOffset: { width: 1, height: 1 },
          shadowOpacity: 0.4,
          shadowRadius: 3,
          elevation: 5,
          // alignItems:'center',
          // flexDirection:'row'
        }}
      >
        <TimeKeepingComponent {...this.props} />
      </View>
    );
  }
}
