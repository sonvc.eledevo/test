import React, { Component } from "react";
import { View, Text } from 'react-native'
import HomeComponent from "../components/HomeComponent";

export default class HomeContainer extends Component {
    render() {
        return (
            <View>
                <HomeComponent {...this.props} />
            </View>
        )
    }
}

