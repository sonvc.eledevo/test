import React, { Component } from "react";
import { View, Text } from 'react-native'
import { connect } from "react-redux";
import LoginComponent from '../components/LoginComponent'
import * as actions from '../actions/loginAction'

class LoginContainer extends Component {
    render() {
        return (
            <View>
                <LoginComponent {...this.props} />
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        Logins: (data, navigate) => {
            dispatch(actions.loginRequest(data, navigate))
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginContainer)