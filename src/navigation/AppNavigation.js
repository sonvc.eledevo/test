import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import ChatStackScreen from "./ChatNavigation";
import ConstactStackScreen from "./ContactNavigation";
import HomeStackScreen from "./HomeNavigation";
import NotifyStackScreen from "./NotifyNavigation";
import ProfileStackScreen from "./ProfileNavigation";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function AppNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        unmountOnBlur: true,
        tabBarShowLabel: false,
        tabBarStyle: {
          position: "absolute",
          bottom: 0,
          left: 1,
          right: 1,
          elevation: 0,
          backgroundColor: "#ffffff",
          borderTopLeftRadius: 40,
          borderTopRightRadius: 40,
          height: 75,
        },
      }}
    >
      <Tab.Screen
        name="HomeScreen"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <>
              <AntDesign
                style={{ marginTop: 5 }}
                name="home"
                size={30}
                color={focused ? "#FF2E00" : "gray"}
              />
            </>
          ),
        }}
      />

      <Tab.Screen
        name="ContactScreen"
        component={ConstactStackScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <>
              <Ionicons
                style={{ marginTop: 5 }}
                name="people-outline"
                size={30}
                color={focused ? "#FF2E00" : "gray"}
              />
            </>
          ),
        }}
      />
      <Tab.Screen
        name="ChatScreen"
        component={ChatStackScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <>
              <Ionicons
                style={{ marginTop: 5 }}
                name="chatbox-ellipses-outline"
                size={30}
                color={focused ? "#FF2E00" : "gray"}
              />
            </>
          ),
        }}
      />
      <Tab.Screen
        name="NotifyScreen"
        component={NotifyStackScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <>
              <Ionicons
                style={{ marginTop: 5 }}
                name="notifications-outline"
                size={30}
                color={focused ? "#FF2E00" : "gray"}
              />
            </>
          ),
        }}
      />

      <Tab.Screen
        name="ProfileScreen"
        component={ProfileStackScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <>
              <SimpleLineIcons
                style={{ marginTop: 5 }}
                name="menu"
                size={30}
                color={focused ? "#FF2E00" : "gray"}
              />
            </>
          ),
        }}
      />
    </Tab.Navigator>
  );
}