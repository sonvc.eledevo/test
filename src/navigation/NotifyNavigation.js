import * as React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as noty from "../screens/NotifyScreen"
const NotifyStack = createNativeStackNavigator();

export default function NotifyStackScreen() {
  return (
    <NotifyStack.Navigator>
      <NotifyStack.Screen name="Notify" component={noty.NotifyScreen} />
      <NotifyStack.Screen name="NotifyDetail" component={noty.NotifyDetailScreen} />
    </NotifyStack.Navigator>
  );
}
