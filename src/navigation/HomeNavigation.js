import * as React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as homes from "../screens/HomeScreen";
import * as rank from "../screens/RankListScreen"
import * as times from "../screens/TimeKeepingScreen"
const HomeStack = createNativeStackNavigator();

export default function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={homes.HomeScreens} options={{ headerShown: false }} />
      <HomeStack.Screen name="Ranklist" component={rank.RankListScreen} />
      <HomeStack.Screen name="RankListDetail" component={rank.RankListDetailScreen} />
      <HomeStack.Screen name="TimeKeepingEdit" component={times.TimeKeppingEditScreen} />
      <HomeStack.Screen name="TimeKeeping" component={times.TimeKeepingScreen} />
      <HomeStack.Screen name="TimeKeepingHistory" component={times.TimeKeepingHistoryScreen} />
    </HomeStack.Navigator>
  );
}
