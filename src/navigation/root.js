import { NavigationContainer } from "@react-navigation/native";
import * as SecureStore from "expo-secure-store";
import React from "react";
import { connect } from "react-redux";
import * as actionTypes from "../contants";
import AppNavigator from "./AppNavigation";
import AuthNavigator from "./AuthenNavigation";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();
const authStack = createNativeStackNavigator();
const appStack = createNativeStackNavigator();
const RootNavigation = (props) => {
  // const { userToken } = this.props
  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await SecureStore.getItemAsync("userToken");
      } catch (e) {
        // Restoring token failed
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      props.restoreToken(userToken);
    };

    bootstrapAsync();
  }, []);
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {props.userToken ? (
          <Stack.Screen
            name="App"
            component={AppNavigator}
            options={{
              headerShown: false,
              // animationTypeForReplace: props.userToken ? 'push' : 'pop'
            }}
          />
        ) : (
          <Stack.Screen
            name="Auth"
            component={AuthNavigator}
            options={{ headerShown: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};
const mapStateToProp = (store) => {
  return {
    userToken: store.auth.userToken,
  };
};
const mapDispatchToProp = (dispatch) => {
  return {
    restoreToken: (userToken) => {
      dispatch({ type: actionTypes.RESTORE_TOKEN, token: userToken });
    },
  };
};

export default connect(mapStateToProp, mapDispatchToProp)(RootNavigation);
