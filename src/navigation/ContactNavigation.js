import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as contacts from "../screens/ContactScreen"
const ContactStack = createNativeStackNavigator();

export default function ConstactStackScreen() {
  return (
    <ContactStack.Navigator>
      <ContactStack.Screen name="Contact" component={contacts.ContactScreen} />
      <ContactStack.Screen name="ContactDetail" component={contacts.ContactDetailScreen} />
      <ContactStack.Screen name="OSCompany" component={contacts.OnsiteCompanyList} />
    </ContactStack.Navigator>
  );
}