import * as React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as chats from "../screens/ChatScreen"
const ChatStack = createNativeStackNavigator();

export default function ChatStackScreen() {
  return (
    <ChatStack.Navigator>
      <ChatStack.Screen name="Chat" component={chats.ChatScreen} />
      <ChatStack.Screen name="Inbox" component={chats.InboxScreen} />
    </ChatStack.Navigator>
  );
}
