import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import * as authenss from "../screens/AuthenScreen"

const Stack = createNativeStackNavigator();

export default function AuthNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LoginScreen"
        component={authenss.LoginScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="RegisterScreen"
        component={authenss.RegisterScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgotPasswordScreen"
        component={authenss.ForgotPasswordScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OtpScreen"
        component={authenss.OtpScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NewPasswordScreen"
        component={authenss.NewPasswordScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
