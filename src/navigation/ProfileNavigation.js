import * as React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as profiles from "../screens/ProfileScreen";
const ProfileStack = createNativeStackNavigator();

export default function ProfileStackScreen() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen name="Profile" component={profiles.ProfileScreen} />
      <ProfileStack.Screen name="BankAcount" component={profiles.BankAcountScreen} />
      <ProfileStack.Screen name="Contract" component={profiles.ContractScreen} />
      <ProfileStack.Screen name="ProfileDetail" component={profiles.ProfileDetailScreen} />
      <ProfileStack.Screen name="RuleOnSite" component={profiles.RuleOnSiteScreen} />
      <ProfileStack.Screen name="CurriculumVitae" component={profiles.CurriculumVitaeScreen} />
    </ProfileStack.Navigator>
  );
}
