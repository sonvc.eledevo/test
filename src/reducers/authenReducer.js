import * as constants from '../contants'

const DEFAULT_STATE = {
    isLoading: true,
    userToken: null,
    isSignOut: false,
    error: false,
    errorMessage: null,
    listAcount: []
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case constants.RESTORE_TOKEN:
            return {
                ...state,
                userToken: action.token,
                isLoading: false,
            }
        case constants.LOGIN_ACCOUNT_REQUEST:
        case constants.REGESTER_ACCOUNT_REQUEST:
        case constants.FORGOT_PASSWORD_REQUEST:
        case constants.NEW_PASSWORD_REQUEST:
        case constants.OTP_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        case constants.LOGIN_ACCOUNT_SUCCESS:
        case constants.REGESTER_ACCOUNT_SUCCESS:
        case constants.FORGOT_PASSWORD_SUCCESS:
        case constants.NEW_PASSWORD_SUCCESS:
        case constants.OTP_SUCCESS:
            return {
                ...state,
                isLoading: false,
                userToken: action.payload
            }
        case constants.LOGIN_ACCOUNT_FAILURE:
        case constants.REGESTER_ACCOUNT_FAILURE:
        case constants.FORGOT_PASSWORD_FAILURE:
        case constants.NEW_PASSWORD_FAILURE:
        case constants.OTP_FAILURE:
            return {
                ...state,
                error: true,
                errorMessage: action.payload.message
            }
        default:
            return state
    }
}