import { combineReducers } from "redux";
import rootReducer from './authenReducer'

export default combineReducers({
    auth: rootReducer
})