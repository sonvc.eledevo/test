export const RESTORE_TOKEN = "RESTORE_TOKEN";

export const LOGIN_ACCOUNT_SUCCESS = "LOGIN_ACCOUNT_SUCCESS";
export const LOGIN_ACCOUNT_FAILURE = "LOGIN_ACCOUNT_FAILURE";
export const LOGIN_ACCOUNT_REQUEST = "LOGIN_ACCOUNT_REQUEST";

export const REGESTER_ACCOUNT_SUCCESS = "REGESTER_ACCOUNT_SUCCESS";
export const REGESTER_ACCOUNT_FAILURE = "REGESTER_ACCOUNT_FAILURE";
export const REGESTER_ACCOUNT_REQUEST = "REGESTER_ACCOUNT_REQUEST";

export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const FORGOT_PASSWORD_FAILURE = "FORGOT_PASSWORD_FAILURE";
export const FORGOT_PASSWORD_REQUEST = "FORGOT_PASSWORD_REQUEST";

export const NEW_PASSWORD_SUCCESS = "NEW_PASSWORD_SUCCESS";
export const NEW_PASSWORD_FAILURE = "NEW_PASSWORD_FAILURE";
export const NEW_PASSWORD_REQUEST = "NEW_PASSWORD_REQUEST";

export const OTP_SUCCESS = "OTP_SUCCESS";
export const OTP_FAILURE = "OTP_FAILURE";
export const OTP_REQUEST = "OTP_REQUEST";
//------------------ Button --------------//
export const BTN_AUTHEN = { width: 308, height: 44 };
export const BTN_SIGNIN = { width: 126, height: 48 };
export const BTN_AUTHEN_CONFIRM = { width: '100%', height: 48 };
export const BTN_CONFIRM = { width: 136, height: 48 };
export const BTN_LEVEL = { width: 66, height: 27 };
export const BTN_MONTH = { width: "auto", height: 48 };
export const BTN_IN = { width: 154, height: 44 };
export const BTN_TIMEKEEPING = { width: 123, height: 48 };
export const BTN_BANK = { width: 315, height: 48 };
export const BTN_CLOSE = { width: 291, height: 48 };
export const BTN_PROFILE = { width: 320 , height: 48 };
export const BTN_NOTYFY = { width: 308 , height: 100 };

//------------------ input --------------//
export const IPT_AUTHEN ={width : 307, height: 50}
export const IPT_SEARCH ={width : 312, height: 60}
export const IPT_CHANGEPASS ={width : 316, height: 50}
