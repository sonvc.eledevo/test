import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import HomeContainer from '../../containers/HomeContainer';
import RankListContainer from '../../containers/RankListContainer';
import SalaryContainer from '../../containers/SalaryContainer';
import TimeKeepingContainer from '../../containers/TimeKeepingContainer';
import * as actionTypes from '../../contants';

function HomeScreen(props) {
    return (
        <View>
            <HomeContainer {...props} />
            <View style={{ marginTop: 20, paddingLeft: "4%", paddingRight: "4%", }}>
                <TimeKeepingContainer {...props} />
                <View style={{ marginBottom: 700 }}>
                    <ScrollView showsVerticalScrollIndicator={false} >
                        <SalaryContainer {...props} />
                        <RankListContainer {...props} />
                    </ScrollView>
                </View>
            </View>
        </View>
    )
}
const mapDispatchToProp = (dispatch) => {
    return {
        logOut: () => {
            dispatch({ type: actionTypes.RESTORE_TOKEN, token: null });
        }
    }
}
export default connect(null, mapDispatchToProp)(HomeScreen)
const styles = StyleSheet.create({})
