import { Button, View } from 'react-native'
import React from 'react'

export default function TimeKeepingHistoryScreen(props) {
    const handleEdit = () => {
        const { navigation } = props;
        navigation.navigate("TimeKeepingEdit");
    }
    return (
        <View>
            <Button
                title='TimeKeppingEdit'
                onPress={() => handleEdit()}
            >
            </Button>
        </View>
    )

}

