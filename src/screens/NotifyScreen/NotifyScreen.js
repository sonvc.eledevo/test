import { Text, StyleSheet, View, Button } from 'react-native'
import React, { Component } from 'react'

export default function NotifyScreen(props) {
    const NotifyHandle = () => {
        const { navigation } = props;
        navigation.navigate("NotifyDetail");
    }

    return (
        <View>
            <Text>NotifyScreen 123</Text>
            <Button
                onPress={() => NotifyHandle()}
                title='notifyDetail'></Button>
        </View>
    )

}

const styles = StyleSheet.create({})