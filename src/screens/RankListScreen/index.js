import RankListScreen from "./RankListScreen";
import RankListDetailScreen from "./RankListDetailScreen";

export {
    RankListScreen,
    RankListDetailScreen
}