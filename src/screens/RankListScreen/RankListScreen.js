import React from "react";
import { Button, View } from "react-native";
import { connect } from "react-redux";
import RankListContainer from "../../containers/RankListContainer";

function RankListScreen(props) {
    const RankListDetail = () => {
        const { navigation } = props;
        navigation.navigate("RankListDetail");
    };

    return (
        <View>
            <RankListContainer {...props} />
            <Button title="RankListDetail" onPress={() => RankListDetail()}></Button>
        </View>
    );
}
export default connect(null, null)(RankListScreen);

