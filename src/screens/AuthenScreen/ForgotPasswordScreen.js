import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ForgotPasswordContainer from '../../containers/ForgotPasswordContainer'
export default function ForgotPasswordScreen(props) {
  return (
    <View>
      <ForgotPasswordContainer {...props} />
    </View>
  )
}

const styles = StyleSheet.create({})