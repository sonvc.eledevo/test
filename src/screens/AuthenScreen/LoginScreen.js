import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import LoginContainer from '../../containers/LoginContainer'
export default function LoginScreen(props) {
  return (
    <View>
      <LoginContainer {...props} />
    </View>
  )
}

const styles = StyleSheet.create({})