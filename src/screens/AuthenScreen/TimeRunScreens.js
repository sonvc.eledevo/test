import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import TimeRunContainer from '../../containers/TimeRunContainer'
export default function TimeRunScreens(props) {
  return (
    <View>
      <TimeRunContainer  {...props}/>
    </View>
  )
}

const styles = StyleSheet.create({})