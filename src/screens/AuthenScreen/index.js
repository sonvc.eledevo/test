import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import ForgotPasswordScreen from "./ForgotPasswordScreen";
import TimeRunScreens from "./TimeRunScreens";
import OtpScreen from "./OtpScreen";
import NewPasswordScreen from "./NewPasswordScreen";;
export {
    RegisterScreen,
    LoginScreen,
    ForgotPasswordScreen,
    TimeRunScreens,
    NewPasswordScreen,
    OtpScreen
}