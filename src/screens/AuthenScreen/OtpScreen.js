import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import OtpContainer from '../../containers/OtpContainer'

export default function OtpScreen(props) {
  return (
    <View>
      <OtpContainer {...props} />
    </View>
  )
}

const styles = StyleSheet.create({})