import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import RegisterContainer from '../../containers/RegisterContainer'
export default function RegisterScreen(props) {
  return (
    <View>
      <RegisterContainer {...props} />
    </View>
  )
}

const styles = StyleSheet.create({})