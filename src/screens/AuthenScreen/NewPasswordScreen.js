import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import NewPasswordContainer from '../../containers/NewPasswordContainer'
export default function NewPasswordScreen(props) {
  return (
    <View>
      <NewPasswordContainer {...props} />
    </View>
  )
}

const styles = StyleSheet.create({})