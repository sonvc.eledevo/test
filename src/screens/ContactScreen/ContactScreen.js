import { Text, StyleSheet, View, Button } from 'react-native'
import React, { Component } from 'react'

export default function ContactScreen(props) {
  const ContactHandle = () => {
    const { navigation } = props;
    navigation.navigate("ContactDetail");
  }
  const OnSiteHandle = () => {
    const { navigation } = props;
    navigation.navigate("OSCompany");
  }

  return (
    <View>
      <Text>ContactScreen 123</Text>
      <Button
        onPress={() => ContactHandle()}
        title='detail'></Button>
      <Button
        onPress={() => OnSiteHandle()}
        title='OnSite'></Button>


    </View>
  )

}

const styles = StyleSheet.create({})