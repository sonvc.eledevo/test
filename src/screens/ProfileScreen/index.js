import BankAcountScreen from "./BankAcountScreen";
import ContractScreen from "./ContractScreen";
import ProfileDetailScreen from "./ProfileDetailScreen";
import ProfileScreen from "./ProfileScreen";
import RuleOnSiteScreen from "./RuleOnSiteScreen";
import CurriculumVitaeScreen from "./CurriculumVitaeScreen";
export {
    BankAcountScreen,
    ContractScreen,
    ProfileDetailScreen,
    ProfileScreen,
    RuleOnSiteScreen,
    CurriculumVitaeScreen
}