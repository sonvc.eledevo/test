import React from "react";
import { Button, Text, View } from "react-native";
import * as SecureStore from 'expo-secure-store';
import { connect } from 'react-redux';
import * as actionTypes from '../../contants';

function ProfileScreen(props) {
  const ProfileDetail = () => {
    const { navigation } = props;
    navigation.navigate("ProfileDetail");
  };
  const HD = () => {
    const { navigation } = props;
    navigation.navigate("Contract");
  };
  const Cv = () => {
    const { navigation } = props;
    navigation.navigate("CurriculumVitae");
  };
  const NoiQuy = () => {
    const { navigation } = props;
    navigation.navigate("RuleOnSite");
  };
  const AcountBankHandle = () => {
    const { navigation } = props;
    navigation.navigate("BankAcount");
  }

  return (
    <View>
      <Text>ProfileScreen</Text>

      <Button title="ProfileDetail" onPress={() => ProfileDetail()}></Button>
      <Button title="Contract" onPress={() => HD()}></Button>
      <Button title="CV" onPress={() => Cv()}></Button>
      <Button title="Nội Quy" onPress={() => NoiQuy()}></Button>
      <Button title="Tài Khoản Ngân Hàng" onPress={() => AcountBankHandle()}></Button>

      <Button
        onPress={() => {
          SecureStore.deleteItemAsync('userToken');
          props.logOut()
        }
        }
        title="Log out"></Button>

    </View>
  );
}
const mapDispatchToProp = (dispatch) => {
  return {
    logOut: () => {
      dispatch({ type: actionTypes.RESTORE_TOKEN, token: null });
    }
  }
}
export default connect(null, mapDispatchToProp)(ProfileScreen);


