import { Text, StyleSheet, View,Button } from 'react-native'
import React, { Component } from 'react'

export default function ChatScreen(props) {
  const ChatHandle = () => {
    const { navigation } = props;
    navigation.navigate("Inbox");
  }
  return (
    <View>
      <Text>ChatScreen 123</Text>
      <Button
        onPress={() => ChatHandle()}
        title='Inbox'></Button>
    </View>
  )
}

const styles = StyleSheet.create({})