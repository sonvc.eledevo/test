export const setLocalStorage = (key, data) => {
    localStorage.setItem(key, data)
}

export const getLocalStorage = (key) => {
    let result = localStorage.getItem(key)
    return JSON.parse(result)
}

export const removetLocalStorage = (key) => {
    localStorage.removeItem(key)
}   