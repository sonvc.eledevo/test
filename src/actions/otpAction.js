import * as types from '../contants'

export const otpRequest = (data, navigate) => {
    return {
        type: types.OTP_REQUEST,
        payload: { data, navigate }
    }
}