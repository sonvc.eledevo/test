import * as types from '../contants'

export const forgotPassRequest = (data, navigate) => {
    return {
        type: types.FORGOT_PASSWORD_REQUEST,
        payload: { data, navigate }
    }
}