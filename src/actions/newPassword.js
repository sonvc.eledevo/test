import * as types from '../contants'

export const newPassRequest = (data, navigate) => {
    return {
        type: types.NEW_PASSWORD_REQUEST,
        payload: { data, navigate }
    }
}