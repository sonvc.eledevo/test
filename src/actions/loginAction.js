import * as types from '../contants'

export const loginRequest = (data, navigate) => {
    return {
        type: types.LOGIN_ACCOUNT_REQUEST,
        payload: { data, navigate }
    }
}

export const registerRequest = (data) => {
    return {
        type: types.REGESTER_ACCOUNT_REQUEST,
        payload: (data)
    }
}