import { MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import {
  Keyboard, ScrollView, Text, TouchableWithoutFeedback, View
} from "react-native";
import styles from "../../assets/Style/StyleRegister";
import * as color from "../commons/Color";
import { ButtonCommon } from "../commons/ui/Button";
import { ButtonBack } from "../commons/ui/ButtonBack";
import { TextInputCommon } from "../commons/ui/TextInput";
import * as size from "../contants";
export default function RegisterComponent(props) {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [code, setCode] = useState("");
  const [confirmpassword, setconfirmpassword] = useState("");
  const [telephone, setTelephone] = useState("");
  const BackButton = () => {
    const { navigation } = props;
    navigation.navigate("LoginScreen");
  };
  const registerHandeler = () => {
    const { navigation } = props;
    navigation.navigate("OtpScreen");
  };
  return (
    <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ButtonBack
            icon={
              <MaterialIcons
                name="keyboard-backspace"
                size={35}
                color="white"
              />
            }
            onPress={() => BackButton()}
          ></ButtonBack>
          
          <View>
            <Text style={styles.title}>Tạo tài khoản</Text>
          </View>

          <TextInputCommon
            text={"Họ và tên"}
            value={fullName}
            onChangeText={(fullName) => setFullName({ fullName })}
          ></TextInputCommon>

          <TextInputCommon
            text={"Email"}
            value={email}
            keyboardType={"email-address"}
            onChangeText={(email) => setEmail({ email })}
          ></TextInputCommon>

          <TextInputCommon
            text={"Mật khẩu"}
            value={password}
            secureTextEntry={true}
            onChangeText={(password) => setPassword(password)}
          ></TextInputCommon>

          <TextInputCommon
            text={"Nhập lại mật khẩu"}
            value={confirmpassword}
            secureTextEntry={true}
            onChangeText={(confirmpassword) =>
              setconfirmpassword(confirmpassword)
            }
          ></TextInputCommon>

          <TextInputCommon
            text={"Số điện thoại"}
            style={styles.textInput}
            value={telephone}
            onChangeText={(telephone) => setTelephone(telephone)}
          ></TextInputCommon>

          <TextInputCommon
            text={"Mã giới thiệu"}
            value={code}
            onChangeText={(code) => setCode(code)}
          ></TextInputCommon>

          <View style={{ alignItems: "flex-end", marginTop: "10%" }}>
            <ButtonCommon
              backgroundColor={color.Black}
              width={size.BTN_SIGNIN.width}
              height={size.BTN_SIGNIN.height}
              color={color.White}
              onPress={() => registerHandeler()}
              title={"Đăng ký"}
            ></ButtonCommon>
          </View>
          <View
            style={styles.titalFooter}>
            <Text>Power by ELEDEVO</Text>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </LinearGradient>
  );
}

