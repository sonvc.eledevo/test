import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { Text, View } from "react-native";
import styles from "../../assets/Style/StyleHome";
import * as color from "../commons/Color";
import { ButtonCommon } from "../commons/ui/Button";
import * as size from "../contants";

export default function HomeComponent(props) {
  const TimekeepingHandle = () => {
    const { navigation } = props;
    navigation.navigate("TimeKeeping");
  };
  return (
    <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
      <View style={styles.topOne}>
        <Text style={styles.textHeader}>Xin Chào, Long</Text>
        <Text
          style={styles.textMGT}>
          Mã giới thiệu: PQDT234561
        </Text>
      </View>
      <View style={styles.headerTwo}>
        <View>
          <Text style={styles.textTN}>
            Thu nhập chính tháng ... dự kiến
          </Text>
          <Text
            style={styles.textSalary}> 12.555.5555 vnđ
          </Text>
        </View>
        <View style={styles.btnCC}>
          <ButtonCommon
            bold={"bold"}
            backgroundColor={color.Black}
            color={color.White}
            width={size.BTN_TIMEKEEPING.width}
            height={size.BTN_TIMEKEEPING.height}
            onPress={() => alert("Chấm Công")}
            title={"Chấm công"}
          ></ButtonCommon>
        </View>
      </View>
    </LinearGradient>
  );
}

