import React, { useState } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import styles from "../../assets/Style/StyleForgotPassword";
import * as color from "../commons/Color";
import * as size from "../contants";
import { ButtonCommon } from "../commons/ui/Button";
import { TextInputCommon } from "../commons/ui/TextInput";
import { ButtonBack } from "../commons/ui/ButtonBack";
export default function LoginComponent(props) {
  const [email, setEmail] = useState("");
  const FogotHandeler = () => {
    const { navigation } = props;
    navigation.navigate("OtpScreen");
  };
  const BackButton = () => {
    const { navigation } = props;
    navigation.navigate("LoginScreen");
  };
  return (
    <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
      <ButtonBack
        icon={
          <MaterialIcons
            name="keyboard-backspace"
            size={35}
            color="white" />} onPress={() => BackButton()}></ButtonBack>
      <View>
        <Text style={styles.title}>Quên mật khẩu </Text>
        <Text style={styles.text1}>
          Nhập email bạn đã đăng ký trên app. Chúng tôi sẽ gửi cho bạn mã OTP
          xác thực để cài lại mật khẩu cho bạn.
        </Text>
      </View>
      <View>
        <TextInputCommon
          text={"Email"}
          keyboardType={"email-address"}
          onChangeText={(email) => setEmail(email)}
          value={email}
        ></TextInputCommon>
      </View>
      <View style={styles.bt}>
        <ButtonCommon
          backgroundColor={color.Black}
          width={size.BTN_AUTHEN_CONFIRM.width}
          height={size.BTN_AUTHEN_CONFIRM.height}
          color={color.White}
          onPress={() => FogotHandeler()}
          title={"Xác Nhận"} >
        </ButtonCommon>
      </View>
    </LinearGradient>
  );
}

