import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import * as color from "../commons/Color";
import { ButtonCommon } from "../commons/ui/Button";
import * as size from "../contants";
import { LinearGradient } from "expo-linear-gradient";

export default function TimeKeepingComponent(props) {
  const [isSelected, setSelected] = useState(0);

  const DATA = [
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
      title: "Tháng này",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
      title: "Tháng trước",
    },
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba1",
      title: "T7/2022",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f632",
      title: "T6/2022",
    },
    {
      id: "58694a0f-3da1-471f-bd96-145571e29d723",
      title: "T5/2022",
    },
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba4",
      title: "T4/2022",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f635",
      title: "T3/2022",
    },
    {
      id: "58694a0f-3da1-471f-bd96-145571e29d726",
      title: "T2/2022",
    },
  ];

  handleSelection = (id) => {
    setSelected(id);
    const { navigation } = props;
    navigation.navigate("TimeKeepingHistory");
  };
  const renderItem = ({ item, index }) => {
    return (
      <View style={{ marginRight: 10 }}>
        <LinearGradient
          colors={
            index === isSelected ? ["#FF9922", "#EE2C2C"] : ["white", "white"]
          }
          style={{ borderRadius: 10 }}
        >
          <ButtonCommon
            bold={index === isSelected ? "bold" : "normal"}
            width={size.BTN_MONTH.width}
            height={size.BTN_MONTH.height}
            color={index === isSelected ? color.White : color.Gray}
            onPress={() => handleSelection(index)}
            title={item.title}
          ></ButtonCommon>
        </LinearGradient>
      </View>
    );
  };
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      data={DATA}
      extraData={DATA}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
    />
  );
}

const styles = StyleSheet.create({});
