import { Text, View, Button, TextInput } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import React, { useRef } from "react";
import { useState } from "react";
import * as color from "../commons/Color";
import * as size from "../contants";
import { ButtonCommon } from "../commons/ui/Button";
import TimeRunComponent from "./TimeRunComponent";
import { LinearGradient } from "expo-linear-gradient";
import { ButtonBack } from "../commons/ui/ButtonBack";
import styles from "../../assets/Style/StyleOtp";
export default function OtpComponent(props) {
  const firstInput = useRef();
  const secondInput = useRef();
  const thirdInput = useRef();
  const fourthInput = useRef();
  const fifthInput = useRef();
  const sixthInput = useRef();
  const [otp, setOtp] = useState([""]);
  const otpHandeler = () => {
    const { navigation } = props;
    navigation.navigate("NewPasswordScreen");
  };
  const BackButton = () => {
    const { navigation } = props;
    navigation.navigate("RegisterScreen");
  };
  return (
    <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
      <ButtonBack
        icon={
          <MaterialIcons
            name="keyboard-backspace"
            size={35}
            color="white" />} onPress={() => BackButton()}></ButtonBack>
      <View>
        <Text style={styles.title11}>Mã OTP</Text>
        <View style={styles.otpContainer}>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={firstInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 1: text });
                text && secondInput.current.focus();
              }}
            />
          </View>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={secondInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 2: text });
                text ? thirdInput.current.focus() : firstInput.current.focus();
              }}
            />
          </View>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={thirdInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 3: text });
                text
                  ? fourthInput.current.focus()
                  : secondInput.current.focus();
              }}
            />
          </View>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={fourthInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 4: text });
                text ? fifthInput.current.focus() : thirdInput.current.focus();
              }}
            />
          </View>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={fifthInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 5: text });
                text ? sixthInput.current.focus() : fourthInput.current.focus();
              }}
            />
          </View>
          <View style={styles.otpBox}>
            <TextInput
              style={styles.otpText}
              keyboardType="number-pad"
              maxLength={1}
              ref={sixthInput}
              onChangeText={(text) => {
                setOtp({ ...otp, 6: text });
                !text && fifthInput.current.focus();
              }}
            />
          </View>
        </View>

        <View style={styles.title}>
          <Text style={styles.text}>
            Mã OTP sẽ hết hạn sau     <TimeRunComponent style={styles.Send} />giây
          </Text>
        </View>
        <View
          style={[
            styles.btnOtp,
            {
              backgroundColor:
                otp.length === 6
                  ? { backgroundColor: "black" }
                  : { backgroundColor: "gray" },
            },
          ]}
        >
          <ButtonCommon
            backgroundColor={color.Black}
            width={size.BTN_AUTHEN_CONFIRM.width}
            height={size.BTN_AUTHEN_CONFIRM.height}
            color={color.White}
            onPress={() => otpHandeler()}
            title={"Xác Nhận"} >
          </ButtonCommon>
        </View>
      </View>
    </LinearGradient>
  );
}

