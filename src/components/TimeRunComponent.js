import React, { Component } from "react";
import { Button, Text, View } from "react-native";

export default class TimeRunComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 2,
      isNeedSend: false,
    };
    const todoTask = () => {
      if (this.state.number == 0) {
        this.setState({ isNeedSend: true });
        return false;
      }
      this.setState((previousState) => {
        return {
          number: previousState.number - 1,
        };
      });
    };
    setInterval(todoTask, 1000);
  }

  render() {
    return (
      <View style={{
          display:"flex",
          alignItems:"center"
      }}>
        <Text
          style={{
            position: "relative",
            fontSize: 16,
            textAlign: "center",
            color: "white",
            fontWeight: "bold",
            top:"40%",
            width:"100%"
          }}
        >
          {!this.state.isNeedSend ? (
            this.state.number
          ) : (
            <Button
              onPress={() => {
                this.setState({
                  number: 2,
                  isNeedSend: false,
                });
              }}
              title="Gửi lại"
              color={"white"}
            ></Button>
          )}
        </Text>
      </View>
    );
  }
}
