import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { ButtonBack } from "../commons/ui/ButtonBack";
import * as color from "../commons/Color";
import * as size from "../contants";
import { ButtonCommon } from "../commons/ui/Button";
import { TextInputCommon } from "../commons/ui/TextInput";
import { Text, View } from "react-native";
import styles from "../../assets/Style/StyleNewPassword";
export default function NewPasswordComponent(props) {
  const [password, setPassword] = useState("");
  const [passwordNew, setPasswordNew] = useState("");
  const BackButton = () => {
    const { navigation } = props;
    navigation.navigate("OtpScreen");
  };
  const NewPassHandeler = () => {
    const { navigation } = props;
    navigation.navigate("LoginScreen");
  };
  return (
    <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
      <ButtonBack
        icon={
          <MaterialIcons
            name="keyboard-backspace"
            size={35}
            color="white" />} onPress={() => BackButton()}></ButtonBack>
      <View>
        <Text style={styles.title}>Đặt lại mật khẩu</Text>
        <Text style={styles.text1}>Nhập mật khẩu mới</Text>
      </View>
      <View>
        <TextInputCommon
          text={"Password"}
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
          value={password}
        ></TextInputCommon>
      </View>
      <View>
        <TextInputCommon
          text={"New Pasword"}
          secureTextEntry={true}
          onChangeText={(passwordNew) => setPassword(passwordNew)}
          value={passwordNew}
        ></TextInputCommon>
      </View>
      <View style={styles.bt}>
        <ButtonCommon
          backgroundColor={color.Black}
          width={size.BTN_AUTHEN_CONFIRM.width}
          height={size.BTN_AUTHEN_CONFIRM.height}
          color={color.White}
          onPress={() => NewPassHandeler()}
          title={"Xác Nhận"} >
        </ButtonCommon>
      </View>
    </LinearGradient>
  );
}

