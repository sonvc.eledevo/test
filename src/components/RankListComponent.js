import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { SafeAreaView, ScrollView, Text, View } from "react-native";
import * as color from "../commons/Color";
import { ButtonCommon } from "../commons/ui/Button";
import CardRankList from "../commons/ui/CardRankList";
import * as size from "../contants";
import styles from "../../assets/Style/StyleRankList";
export default function RankListComponent(props) {
  const rankListBTN = () => {
    const { navigation } = props;
    navigation.navigate("RankListDetail");
  };

  const viewALL = () => {
    const { navigation } = props;
    navigation.navigate("Ranklist");
  };
  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ marginTop: "6%", paddingBottom: 140 }}>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={styles.textSmail}>Doanh thu theo cấp</Text>
            <Text style={styles.textSmail} onPress={() => viewALL()}>
              Xem tất cả
            </Text>
          </View>

          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 1"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>6.560.000 vnđ</Text>
                  <Text style={styles.textSmail3}>3 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 2"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>3,560,000 vnđ</Text>
                  <Text style={styles.textSmail3}>8 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>

          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 3"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>1,560,000 vnđ</Text>
                  <Text style={styles.textSmail3}>15 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 4"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>1,260,000 vnđ</Text>
                  <Text style={styles.textSmail3}>18 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 5"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>960,000 vnđ</Text>
                  <Text style={styles.textSmail3}>22 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 6"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>740,000 vnđ</Text>
                  <Text style={styles.textSmail3}>28 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
          <View style={styles.sumView}>
            <CardRankList>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <LinearGradient
                  colors={["#FF9922", "#EE2C2C"]}
                  style={{ borderRadius: 10 }}
                >
                  <ButtonCommon
                    width={size.BTN_LEVEL.width}
                    height={size.BTN_LEVEL.height}
                    color={color.White}
                    onPress={() => rankListBTN()}
                    title={"Cấp 7"}
                  ></ButtonCommon>
                </LinearGradient>

                <View style={styles.sumViewSmall}>
                  <Text style={styles.textRL}>560,000 vnđ</Text>
                  <Text style={styles.textSmail3}>32 người</Text>
                </View>
              </View>
            </CardRankList>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

