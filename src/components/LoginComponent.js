import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Pressable,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import styles from "../../assets/Style/StyleLogin";
import * as color from "../commons/Color";
import * as size from "../contants";
import { ButtonCommon } from "../commons/ui/Button";
import { TextInputCommon } from "../commons/ui/TextInput";

export default function LoginComponent(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const loginHandeler = () => {
    const { navigation } = props;
    // navigation.navigate('RegisterScreen')
    let data = {
      email: email,
      password: password,
    };
    props.Logins(data, navigation);
  };
  const gotoregisterHandeler = () => {
    const { navigation } = props;
    navigation.navigate("RegisterScreen");
  };
  const forgotPassHandeler = () => {
    const { navigation } = props;
    navigation.navigate("ForgotPasswordScreen");
  };
  return (
    <View style={styles.container}>
      <LinearGradient colors={["#FF9922", "#EE2C2C"]} style={styles.gradient}>
        <KeyboardAvoidingView
          keyboardVerticalOffset={10}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <TouchableWithoutFeedback
            onPress={Keyboard.dismiss}
            accessible={false}
          >
            <View style={styles.containerSmaill}>
              <Text style={styles.title}>Đăng nhập</Text>
              <Text style={styles.text1}>Chào mừng bạn đến với ELEDEVO!</Text>
              <View>
                <TextInputCommon
                  text={"Email"}
                  keyboardType={"email-address"}
                  onChangeText={(email) => setEmail(email)}
                  value={email}
                ></TextInputCommon>
                <TextInputCommon
                  text={"Password"}
                  secureTextEntry={true}
                  onChangeText={(password) => setPassword(password)}
                  value={password}
                ></TextInputCommon>
              </View>
              <View style={styles.btLG}>
                <ButtonCommon
                  backgroundColor={color.Black}
                  width={size.BTN_SIGNIN.width}
                  height={size.BTN_SIGNIN.height}
                  color={color.White}
                  onPress={() => loginHandeler()}
                  title={"Login"}
                ></ButtonCommon>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </LinearGradient>
      <View style={styles.footerChild}>
        <Pressable onPress={() => gotoregisterHandeler()}>
          <Text>Đăng ký tài khoản | </Text>
        </Pressable>
        <Pressable onPress={() => forgotPassHandeler()}>
          <Text>Quên mật khẩu</Text>
        </Pressable>
      </View>
    </View>
  );
}