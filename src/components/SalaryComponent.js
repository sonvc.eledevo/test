import React from "react";
import { Text, View } from "react-native";
import Cart from '../commons/ui/Card';
import styles from "../../assets/Style/StyleSalary";
export default function SalaryComponent(props) {

    return (
        <View style={{ marginTop: "6%" }}>
            <Cart>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <View>
                        <Text style={styles.textTN}>Thu nhập thụ động T9 dự kiến</Text>
                    </View>
                    <View>
                        <Text style={styles.textTN}>Tổng cấp dưới</Text>
                    </View>
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <View>
                        <Text style={styles.textVND}>12.560.000 vnđ</Text>
                    </View>
                    <View>
                        <Text style={styles.textVND}>2156</Text>
                    </View>
                </View>
                <View style={{ marginTop: 10 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View>
                            <Text style={styles.textTN}>Tổng thu nhập dự kiến</Text>
                        </View>
                        <View>
                            <Text style={styles.textTN}>Đã kích hoạt</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View>
                            <Text style={styles.textVNDD}>24.560.000 vnđ</Text>
                        </View>
                        <View>
                            <Text style={styles.textVNDD}>1543</Text>
                        </View>
                    </View>
                </View>
            </Cart>
        </View>
    );
}


