import { LinearGradient } from "expo-linear-gradient";

export const
  White = "#FFFFFF",
  Black = "#000000",
  Title = "#2B2B2B",
  Purple = "#C33AA5",
  Green = "#377437",
  Orange = "#F55626",
  Gray = "#C4C4C4",
  TabMenu = "#FF2E00";
