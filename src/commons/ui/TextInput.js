import React, { useMemo, useState } from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";

export const TextInputCommon = (props) => {
  const { text, keyboardType, onChangeText, value, secureTextEntry } = props;
  const [isValue, setValue] = useState(false);
  return (
    <View style={styles.container}>
      {isValue && <Text style={styles.textBtn}>{text}</Text>}
      <View style={styles.input}>
        <TextInput
          style={styles.textInput}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          value={value}
          placeholder={!isValue ? text : ""}
          onPressIn={() => setValue(true)}
          onBlur={() => (value ? setValue(true) : setValue(false))}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginTop: "6%",
  },
  input: {
    backgroundColor: "white",
    marginTop: 5,
    marginBottom: 10,
    borderRadius: 10,
    height: 50,
    paddingLeft: 15,
    paddingRight: 5,
  },
  textInput: {
    // fontWeight: "bold",
    fontSize: 18,
    height: 50,
  },
  textBtn: {
    fontSize: 14,
    color: "white",
    // marginTop: "3%",
  },
});
