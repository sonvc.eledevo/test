import { Text, StyleSheet, View } from "react-native";
import React, { Component } from "react";
export default class CartRankList extends Component {
    render() {
        return (
            <View {...this.props} style={styles.card}>
                {this.props.children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        height: 65,
        // shadowColor: "black",
        // shadowOpacity: 0.26,
        // shadowOffset: { width: 0, height: 1 },
        // shadowRadius: 8,
        // elevation: 5,
        borderRadius: 10,
        backgroundColor: "white",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 8,
        paddingBottom: 8,
        justifyContent: 'center'
    },
});
