import React from "react";
import { StyleSheet, Text, Pressable ,View} from "react-native";
export const ButtonBack = (props) => {
  const { icon, onPress } = props;

  return (
    <View style={styles.backBtn}>
      <Pressable onPress={onPress}>
        <Text>{icon}</Text>
      </Pressable>
    </View>
  );
};
const styles = StyleSheet.create({
  backBtn: {
    marginTop: "20%",
  },
});
