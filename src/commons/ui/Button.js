import React from "react";
import { Pressable, StyleSheet, Text } from "react-native";
export const ButtonCommon = (props) => {
  const { title, onPress, backgroundColor, width, height, color, bold } = props;
  const { button, text } = styles;
  const buttonStyle = StyleSheet.flatten([
    button,
    { backgroundColor: backgroundColor },
    { width: width },
    { height: height },
  ]);
  const textStyle = StyleSheet.flatten([
    text,
    { color: color },
    { fontWeight: bold },
  ]);
  return (
    <Pressable
      style={buttonStyle}
      backgroundColor={backgroundColor}
      width={width}
      height={height}
      onPress={() => {
        onPress();
      }}
    >
      <Text style={textStyle} color={color}>
        {title}
      </Text>
    </Pressable>
  );
};
const styles = StyleSheet.create({
  button: {
    width: 123,
    height: 48,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    // elevation: 3,
    backgroundColor: "black",
  },
  text: {
    fontSize: 14,
    lineHeight: 21,
    fontWeight: "normal",
    letterSpacing: 0.25,
    color: "white",
  },
});
