import { put, takeEvery } from 'redux-saga/effects'
import * as types from '../contants'
import * as addDataApi from '../fetchApis/addDataApi'
import * as SecureStore from 'expo-secure-store';

function* loginHandle(action) {
    try {
        const token = "ACBAH213123"
        yield SecureStore.setItemAsync('userToken', token)
        yield put({
            type: types.LOGIN_ACCOUNT_SUCCESS,
            payload: token
        })
    } catch (error) {
        yield put({
            type: types.LOGIN_ACCOUNT_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* registerHandle(action) {
    try {
        const listRegister = yield addDataApi(action.payload)
        yield put({
            type: types.REGESTER_ACCOUNT_SUCCESS,
            payload: listRegister
        })
        yield put({
            type: types.REGESTER_ACCOUNT_REQUEST,
            payload: listRegister
        })
    } catch (error) {
        yield put({
            type: types.REGESTER_ACCOUNT_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* forgotPasswprdHandle(action) {
    try {
        const forgotPassword = yield addDataApi(action.payload)
        yield put({
            type: types.FORGOT_PASSWORD_REQUEST,
            payload: forgotPassword
        })
        yield put({
            type: types.FORGOT_PASSWORD_SUCCESS,
            payload: forgotPassword
        })

    } catch (error) {
        yield put({
            type: types.FORGOT_PASSWORD_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })

    }
}
function* otpHandle(action) {
    try {
        const otp = yield addDataApi(action.payload)
        yield put({
            type: types.OTP_REQUEST,
            payload: otp
        })
        yield put({
            type: types.OTP_SUCCESS,
            payload: otp
        })
    } catch (error) {
        yield put({
            type: types.OTP_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* newPassHandle(action) {
    try {
        const newPass = yield addDataApi(action.payload)
        yield put({
            type: types.NEW_PASSWORD_REQUEST,
            payload: newPass
        })
        yield put({
            type: types.NEW_PASSWORD_SUCCESS,
            payload: newPass
        })
    } catch (error) {
        yield put({
            type: types.NEW_PASSWORD_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
export const AuthenSaga = [
    takeEvery(types.LOGIN_ACCOUNT_REQUEST, loginHandle),
    takeEvery(types.REGESTER_ACCOUNT_REQUEST, registerHandle),
    takeEvery(types.FORGOT_PASSWORD_REQUEST, forgotPasswprdHandle),
    takeEvery(types.NEW_PASSWORD_REQUEST, newPassHandle),
    takeEvery(types.OTP_REQUEST, otpHandle)
]