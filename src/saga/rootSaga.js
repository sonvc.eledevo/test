import { all } from 'redux-saga/effects'
import { AuthenSaga } from './authenSaga'
function* rootSaga() {
    yield all([
        ...AuthenSaga
    ])
}
export default rootSaga