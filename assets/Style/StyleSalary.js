import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    textTN: {
        fontSize: 14,
        marginTop: 5
    },
    textVND: {
        marginTop: 5,
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'flex-end'
    },
    textVNDD: {
        marginTop: 5,
        color: "#FF4800",
        fontSize: 22,
        fontWeight: 'bold',
    }
});

export default styles