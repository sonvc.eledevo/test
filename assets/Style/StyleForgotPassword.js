import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
    gradient: {
        height: "100%",
        padding: 30,
        width: "100%",
    },
    backBtn: {
        marginTop: 60,
        marginLeft: -330,
    },
    backText: {
        color: "white",
        fontSize: 30,
    },
    textBtn: {
        fontSize: 13,
        color: "#ABABAB",
    },
    title: {
        color: "white",
        fontSize: 36,
        fontWeight: "bold",
        marginTop: 41,
    },
    text: {
        color: "white",
        fontSize: 18,
        marginTop: 48,
        maxWidth: 307,
        width: "100%",
        lineHeight: 30,
    },
    input: {
        backgroundColor: "white",
        marginTop: 50,
        marginBottom: 10,
        borderRadius: 10,
        height: 50,
        padding: 10,
    },
    textInput: {
        fontWeight: "bold",
        fontSize: 16,
    },
    forgotBtn: {
        backgroundColor: "black",
        color: "white",
        width: "100%",
        height: 44,
        borderRadius: 10,
        position: "absolute",
        right: "9%",
        bottom: "45%",
        fontSize: 18,
    },
    text1: {
        color: "white",
        fontSize: 14,
        marginTop: "10%",
        marginBottom: "10%",
    },
    bt: {
        marginTop: "10%"
    }
});
export default styles