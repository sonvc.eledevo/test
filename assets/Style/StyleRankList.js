import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    sumView: {
        marginTop: "3%",
    },
    sumViewSmall: {
        marginTop: "-5%",
    },
    textRL: {
        fontSize: 16,
        fontWeight: "bold",
    },
    textSmail: {
        fontSize: 14,
        color: "#6F6F6F",
    },
    textSmail3: {
        position: "absolute",
        fontSize: 14,
        color: "#6F6F6F",
        right: 0,
        top: 20,
    },
});
export default styles