import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    gradient: {
        height: "100%",
        paddingLeft: "8%",
        paddingRight: "8%",
        width: "100%",
    },
    title: {
        color: "white",
        fontSize: 36,
        fontWeight: "bold",
        // marginTop: "20%",
    },
    text1: {
        color: "white",
        fontSize: 14,
        marginTop: "10%",
        marginBottom: "10%",
    },
    login: {
        backgroundColor: "black",
        color: "white",
        width: 126,
        height: 38,
        borderRadius: 10,
    },
    container: {
        position: "relative",
        alignItems: "center",
    },
    containerSmaill: {
        justifyContent: "space-around",
        paddingTop: "40%"
    },
    footerChild: {
        position: "absolute",
        bottom: "2%",
        flexDirection: "row",
    },
    btLG: {
        alignItems: "flex-end",
        marginTop: "10%"
    }
});

export default styles