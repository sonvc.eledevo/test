import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    textHeader: {
        fontSize: 36,
        fontWeight: "bold",
        color: "white",
    },
    gradient: {
        width: "100%",
        paddingLeft: "4%",
        paddingRight: "4%",
    },
    topOne: {
        marginTop: "20%",
        borderBottomWidth: 0.5,
        borderBottomColor: "white",
    },
    headerTwo: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    textSalary: {
        marginTop: "3%",
        marginBottom: "6%",
        color: "white",
        fontSize: 22,
        fontWeight: "bold",
        shadowColor: "black",
        shadowOffset: { width: 0, height: 4 },
        elevation: 3,
        shadowOpacity: 0.2,
        shadowRadius: 4,
    },
    textMGT: {
        marginTop: "1%",
        marginBottom: "8%",
        color: "white",
        fontSize: 14,
    },
    btnCC: {
        justifyContent: "center",
        alignItems: "center"
    },
    textTN: {
        marginTop: "6%",
        color: "white", fontSize: 14
    }
});

export default styles