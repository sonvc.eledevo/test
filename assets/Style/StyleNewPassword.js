import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
    gradient: {
        height: "100%",
        padding: 30,
        width: "100%",
    },
    backBtn: {
        marginTop: 60,
        marginLeft: -330,
    },
    backText: {
        color: "white",
        fontSize: 30,
    },
    title: {
        color: "white",
        fontSize: 36,
        fontWeight: "bold",
        marginTop: 41,
    },
    text: {
        color: "white",
        fontSize: 18,
        marginTop: 48,
    },
    input: {
        backgroundColor: "white",
        marginTop: 35,
        marginBottom: 10,
        borderRadius: 10,
        height: 50,
        padding: 10,
    },
    textBtn: {
        fontSize: 13,
        color: "#ABABAB",
    },
    changeBtn: {
        backgroundColor: "black",
        color: "white",
        width: "100%",
        height: 44,
        borderRadius: 10,
        position: "absolute",
        right: "9%",
        bottom: "43%",
        fontSize: 18,
    },
    bt: {
        marginTop: "10%"
    },
    text1: {
        color: "white",
        fontSize: 14,
        marginTop: "10%",
        marginBottom: "10%",
    },
});
export default styles