import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    gradient: {
        height: "100%",
        paddingLeft: "8%",
        paddingRight: "8%",
        width: "100%",
    },
    backBtn: {
        marginTop: 85,
        marginLeft: -330,
    },
    backText: {
        color: "white",
        fontSize: 30,
    },
    otpContainer: {
        marginHorizontal: 1,
        marginBottom: 15,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
        marginTop: "15%",
    },
    otpBox: {
        borderColor: "white",
        borderWidth: 2,
        backgroundColor: "white",
        borderRadius: 3,
    },
    otpText: {
        fontSize: 20,
        color: "black",
        padding: 0,
        textAlign: "center",
        paddingHorizontal: 15,
        paddingVertical: 5,
    },
    btnOtp: {
        color: "red",
        borderRadius: 50,
        padding: 60,
        marginTop: "5%",
        justifyContent: "center",
        backgroundColor: "black",
    },
    bt: {
        borderRadius: 30,
    },
    title: {
        display: "flex",
    },
    text: {
        textAlign: "center",
        fontSize: 16,
        color: "#FFFFFF",
        marginTop: "10%",
        paddingLeft: 18,
    },
    Send: {
        backgroundColor: "black",
    },
    changeOtppp: {
        backgroundColor: "black",
        color: "white",
        width: "100%",
        height: 30,
        borderRadius: 10,
        position: "absolute",
        right: "20%",
        bottom: "43%",
        fontSize: 18,
    },
    title11: {
        color: "white",
        fontSize: 36,
        fontWeight: "bold",
        marginTop: '10%'
    },
});
export default styles