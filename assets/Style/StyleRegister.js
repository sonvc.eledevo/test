import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    gradient: {
        height: "100%",
        paddingLeft: "8%",
        paddingRight: "8%",
        width: "100%",
    },
    title: {
        color: "white",
        fontSize: 36,
        fontWeight: "bold",
        marginTop: "5%",
    },
    titalFooter: {
        height: 250,
        flex: 1,
        justifyContent: "flex-end",
        marginBottom: "10%",
        alignItems: "center",
    }
});
export default styles