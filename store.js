import { createStore, applyMiddleware } from 'redux'
import rootReducer from './src/reducers/rootReducer'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './src/saga/rootSaga';
import logger from 'redux-logger'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootReducer,applyMiddleware(sagaMiddleware, logger))
sagaMiddleware.run(rootSaga)
 
export default store;